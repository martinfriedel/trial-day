# Explain the functions

In this task you'll need to read and explain in natural language what a set of functions do

## Notes

    - The functions will be ordered by level of difficulty, the first function will be the easiest
    - The functions are located on the functions file
