# Typescript utility types

There are some scenarios that need to be solved using Typescript utility types.

## Notes

-   The scenarios are located under the utils folder in a file called `typescriptUtilityTypesScenarios.ts`
