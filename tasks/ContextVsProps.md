# Context vs Props

In this task you'll be presented with a situation where you'll have some data in the view which needs to reach the lowest component in the dom tree.

## Notes

-   In this task, there's a page called ContextVsProps that contains some data that needs to reach the lowest (in the dom tree) component that it renders
-   You'll have to use Context to pass the data to the lowest component
