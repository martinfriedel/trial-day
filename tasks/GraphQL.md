# GraphQL queries and mutations

There's a server folder that contains a GraphQL server. This contains a list of ToDo items.
These have to be displayed, a new one can be created and they can't be deleted.
They can have their text edited, and they can be checked as done or not done.

## Notes

-   In this task, there's a page called graphql which will display these todos.
-   You'll have to get the data from the server and display it.
-   You can use any method to get the data from the server.
-   You need to use MUI as the base component library
-   Be as thorough as possible
