# Create a basic Component

Implement a basic component that will display a text input.

-   The user must be able to write in this input and the data must be available for use in the remaining code
-   The user must be able to clear the input with a clear button that has to be inside the input

## Notes

-   In this task, you'll need to create a React Component
    -   It should be a stateless functional component
    -   The props will consist of:
        -   **label**
        -   **onChange**
        -   **onClear**
        -   **value**
-   You need to use MUI as the base component library
-   You need to implement at least one story for the component
-   You need to implement at least one test for the component

-   You can add an **index.ts** file to the component folder, which will be used to import the component

-   Be as thorough as possible
