# Style the Component

There is an existing unstyled component and an image of the final layout. Your task is to style the component to the design in the image.

## Notes

-   In this task you'll be asked to style the component either via CSS or with MUI.
-   Here's the link to see the component: [Figma Link](<https://www.figma.com/file/05iMue7q9CsqxdGG5vKcxm/MUI-for-Figma-v5.4.0-(Community)-(Community)?node-id=5003%3A91231>)
-   The component is called StyleMe
-   Be as thorough as possible.
