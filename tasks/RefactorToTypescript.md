# Refactor to TypeScript

There is some code in this project that is JSX only and should be converted to Typescript. Check the file `client/src/pages/jsxRefactor/JsxRefactor.jsx` and the component used there too.

## Notes

-   Refactor the code to use TypeScript
-   Fix anx bugs you encounter
-   Refactor to functional component
