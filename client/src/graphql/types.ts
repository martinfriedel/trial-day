export type Maybe<T> = T | null
export type InputMaybe<T> = Maybe<T>
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] }
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> }
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> }
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
    ID: string
    String: string
    Boolean: boolean
    Int: number
    Float: number
}

export type Mutation = {
    __typename?: 'Mutation'
    saveTodo: Todo
}

export type MutationSaveTodoArgs = {
    input: NewTodo
}

export type NewTodo = {
    done?: InputMaybe<Scalars['Boolean']>
    id?: InputMaybe<Scalars['ID']>
    text: Scalars['String']
    userID: Scalars['String']
}

export type Query = {
    __typename?: 'Query'
    todos: Array<Todo>
}

export type Todo = {
    __typename?: 'Todo'
    done: Scalars['Boolean']
    id: Scalars['ID']
    text: Scalars['String']
    user: User
}

export type User = {
    __typename?: 'User'
    id: Scalars['ID']
    name: Scalars['String']
}
