import gql from 'graphql-tag'

export default gql`
    mutation saveTodos($input: NewTodo!) {
        saveTodo(input: $input) {
            id
            text
            done
            user {
                id
                name
            }
        }
    }
`
