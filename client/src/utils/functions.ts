interface DataStruct {
    id: string
    name: string
    year: number
}

export const functions: any[] = [
    (a: string, b: string, c: string): string => {
        return a + (c.includes(b.substring(0, 2)) ? '_1' : '_2')
    },
    (data: DataStruct, year: number): boolean | string => {
        if (data.year <= year) {
            return data.name
        } else {
            return false
        }
    },
    (value: string): number | undefined => {
        if (!isNaN(Number(value))) {
            return Number(value)
        }
        return undefined
    },
]
