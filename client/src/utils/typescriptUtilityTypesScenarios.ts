// Scenario 1: Imagine you are developing a web application for a client.
// The client wants to be able to edit some data in a data structure.
// As soon as the client edits something, they want the data to be sent to the server.
// Create a type that allows you to send the minimum amount of data to the server.
// The server always needs to receive the id
// The data structure is as follows:
export interface Scenario1 {
    id: number
    name: string
    age: number
    address: {
        street: string
        city: string
        state: string
        zip: number
    }
    phone: string
    email: string
    likings: string[]
    picture: string
}
// Write your code below
export type Scenario1Response = Partial<Scenario1> & { id: number }

export const scenario1: Scenario1Response = {
    address: {
        street: '123 Main St',
        city: 'Anytown',
        state: 'CA',
        zip: 12345,
    },
    id: 1,
}

// Scenario 2: Imagine you are developing a web application for a client.
// The client wants you to load data from a server.
// You are given the type of the data that the server sends.
// Once the data is loaded, we want to make use of all the fields, even if they weren't returned by the server.
// Create a type that allows you to use these fields.
// (You can suppose any amount of processing is done on the response before assigning it to the type)
// TODO: REWORK THE PHRASING

export interface Scenario2 {
    id: number
    name?: string
    age?: number
    address?: {
        street: string
        city: string
        state: string
        zip: number
    }
    phone?: string
    email?: string
    likings?: string[]
    picture?: string
}

// Write your code below
export type Scenario2Response = Required<Scenario2>

export const scenario2: Scenario2Response = {
    id: 1,
    name: 'John Doe',
    address: {
        street: '123 Main St',
        city: 'Anytown',
        state: 'CA',
        zip: 12345,
    },
    age: 30,
    email: 'test@qubidu.de',
    likings: ['cats', 'dogs'],
    phone: '123-456-7890',
    picture: 'http://via.placeholder.com/350x150',
}

// Scenario 3: Given an enum, create a type where each field is one of the values of the enum.
// Each field will be assigned to number
export enum Scenario3 {
    FIRST = 'FIRST',
    SECOND = 'SECOND',
    THIRD = 'THIRD',
}

// Write your code below
export type Scenario3Response1 = { [key in Scenario3]: number }
export type Scenario3Response2 = Record<Scenario3, number>

export const scenario3: Scenario3Response1 = {
    FIRST: 1,
    SECOND: 2,
    THIRD: 3,
}

// Scenario 4: Given a type which fields' types may change over time
// Create a constant that uses one, and only one, of the fields from the original type
// (You can choose which field to use)
// Make it so that future changes made to the original type will be reflected in the constant's type
// (You can assume the fields will be the same name)
export interface Scenario4 {
    id: number
    name: string
    age: number
    address: {
        street: string
        city: string
        state: string
        zip: number
    }
    phone: string
    email: string
    likings: string[]
    picture: string
}

// Write your code below
export type Scenario4Response = Pick<Scenario4, 'address'>
export const scenario4: Scenario4Response = {
    address: {
        street: '123 Main St',
        city: 'Anytown',
        state: 'CA',
        zip: 12345,
    },
}

// Scenario 5: GraphQL generated types contain extra fields that we have not defined ourselves in the schema, like __typename
// Given a type that contains a __typename field, create a derived type that removes the __typename field
// If the original type changes over time, make sure the derived type changes as well
export interface Scenario5 {
    __typename: string
    id: number
    name: string
    age: number
    address: {
        street: string
        city: string
        state: string
        zip: number
    }
    phone: string
    email: string
    likings: string[]
    picture: string
}

// Write your code below
export type Scenario5Response = Omit<Scenario5, '__typename'>
export const scenario5: Scenario5Response = {
    id: 1,
    name: 'John Doe',
    address: {
        street: '123 Main St',
        city: 'Anytown',
        state: 'CA',
        zip: 12345,
    },
    age: 30,
    email: 'test@qubidu.de',
    likings: ['cats', 'dogs'],
    phone: '123-456-7890',
    picture: 'http://via.placeholder.com/350x150',
}

// ESTABLISHED
// SENIOR
enum SEMAPHORE {
    R = 'R',
    Y = 'Y',
    G = 'G',
}

const allSemaphores = ['A123', 'A124', 'A125'] as const
export type SEM_MAP = Record<typeof allSemaphores[number], { [date: string]: Record<SEMAPHORE, number> }>

export const c: SEM_MAP = {
    A123: {
        '2020-01-01': {
            G: 2,
            R: 1,
            Y: 1,
        },
        '2020-01-02': {
            G: 1,
            R: 1,
            Y: 1,
        },
    },
    A124: {},
    A125: {
        '2020-01-01': {
            G: 1,
            R: 1,
            Y: 1,
        },
    },
}
