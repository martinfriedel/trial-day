export interface GetTestIDReturn {
    [key: string]: string | undefined
}

export const getTestID = (id: string): GetTestIDReturn => {
    if (process.env.NODE_ENV === 'production') return {}
    return {
        'data-testid': id,
    }
}
type COMPONENT = 'EXAMPLE' | 'SEARCH_BAR'
type COMPONENT_ELEMENT =
    | 'BOX'
    | 'LOGOUT_BUTTON'
    | 'LOGIN_BUTTON'
    | 'COUNTER'
    | 'MINUS'
    | 'PLUS'
    | 'SAVE'
    | 'LOAD'
    | 'REDYPLOY_TEST_DATA_BUTTON'
    | 'TEXT_INPUT'
    | 'SEARCH_BUTTON'
type PAGES = 'EXAMPLE'
type PAGES_ELEMENT = 'TEXT'

const TestIDs = {
    GET_COMPONENT: (component: COMPONENT, element: COMPONENT_ELEMENT, category?: string): GetTestIDReturn =>
        getTestID(component + '_COMPONENT_' + element + '_ELEMENT' + (category ? '_' + category : '')),
    GET_PAGES: (page: PAGES, element: PAGES_ELEMENT, category?: string): GetTestIDReturn =>
        getTestID(page + '_PAGE_' + element + '_ELEMENT' + (category ? '_' + category : '')),
}

export default TestIDs
