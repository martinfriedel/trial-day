import { red } from '@mui/material/colors'
import { createTheme, responsiveFontSizes } from '@mui/material/styles'
import fontStyles from 'utils/fontStyles'
import colors from './colors'

export const drawerPadding = 30

// Create a theme instance.
export const theme = createTheme({
    breakpoints: {
        values: {
            xs: 479,
            sm: 767,
            md: 991,
            lg: 1200,
            xl: 1536,
        },
    },
    palette: {
        primary: {
            main: colors.main,
        },
        error: {
            main: red.A400,
        },
    },
    typography: {
        fontSize: 16,
        h1: { ...fontStyles.h1 },
        h2: { ...fontStyles.h2 },
        h3: { ...fontStyles.h3 },
        h4: { ...fontStyles.h4 },
        h5: { ...fontStyles.h5 },
        h6: { ...fontStyles.h6 },
        body1: { ...fontStyles.body1 },
        body2: { ...fontStyles.body2 },
        subtitle1: { ...fontStyles.subtitle1 },
        subtitle2: { ...fontStyles.subtitle2 },
        button: { ...fontStyles.button },
        caption: { ...fontStyles.caption },
    },
})

const ModifiedTheme = responsiveFontSizes(theme)

export default ModifiedTheme
