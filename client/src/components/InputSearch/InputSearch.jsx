import PropTypes from 'prop-types'
import React from 'react'

export class InputSearch extends React.Component {
    constructor() {
        super()
    }

    checkSend = (e) => {
        let keyCode = e.keyCode || e.which
        if (keyCode === 13) {
            this.props.search(true)
        }
    }

    sendSearch = () => {
        this.props.search(true)
        this.input.focus()
    }

    moveCaretAtEnd = (e) => {
        let tempValue = e.target.value
        e.target.value = ''
        e.target.value = tempValue
    }

    render() {
        let classes = ['inputSearch']

        if (this.props.className) {
            classes.push(this.props.className)
        }

        return (
            <div className={classes.join(' ')}>
                <input
                    ref={(c) => (this.input = c)}
                    onChange={this.props.onChange}
                    onFocus={this.moveCaretAtEnd}
                    onKeyPress={this.checkSend}
                    placeholder={'search...'}
                    value={this.props.value}
                    autoFocus={this.props.autoFocus}
                />
                <button onClick={this.sendSearch} className="searchButton">
                    search
                </button>
            </div>
        )
    }
}

InputSearch.propTypes = {
    search: PropTypes.func,
    className: PropTypes.string,
    autoFocus: PropTypes.bool,
    onChange: PropTypes.func,
    value: PropTypes.string,
}
