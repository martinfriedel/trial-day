import { Button, Grid, TextField } from '@mui/material'
import React, { useState } from 'react'
import TestIDs from 'utils/TestIDs'
import useI18n from './components/useI18n'

export interface SearchBarProps {
    onSearch: (search: string) => void
    redeployTestData: () => void
}

const SearchBar = (props: SearchBarProps): JSX.Element => {
    const { t } = useI18n()
    const [search, setSearch] = useState('')

    const { onSearch, redeployTestData } = props

    return (
        <Grid container style={{ marginTop: 16 }} spacing={1}>
            <Grid item xs={12} sm={4}>
                <Button
                    fullWidth
                    onClick={() => redeployTestData()}
                    variant={'contained'}
                    {...TestIDs.GET_COMPONENT('SEARCH_BAR', 'REDYPLOY_TEST_DATA_BUTTON')}
                >
                    {t(`searchBar.deployButtonLabel`)}
                </Button>
            </Grid>
            <Grid container item xs={12} sm={8} spacing={1} alignItems={'center'}>
                <Grid item xs={12} sm={8}>
                    <TextField
                        {...TestIDs.GET_COMPONENT('SEARCH_BAR', 'TEXT_INPUT')}
                        variant={'outlined'}
                        fullWidth
                        size={'small'}
                        value={search}
                        onChange={(e): void => setSearch(e.target.value)}
                        onKeyDown={(e): void => {
                            if (e.key === 'Enter') onSearch(search)
                        }}
                    />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Button
                        fullWidth
                        onClick={() => onSearch(search)}
                        variant={'contained'}
                        {...TestIDs.GET_COMPONENT('SEARCH_BAR', 'SEARCH_BUTTON')}
                    >
                        {t(`searchBar.buttonLabel`)}
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default SearchBar
