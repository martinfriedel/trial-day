import * as React from 'react'
import { Col, Input, Row } from 'reactstrap'
import { CPItemEditComponent, EditState } from './CPItemEditComponent'

export interface BasedataEntryParams {
    Content: string
    ID: number
}

export interface BasedataParams {
    Data: BasedataEntryParams[]
    Name: string
    Key: string
}

export interface CPBaseDataProps {
    baseDataFromServer: BasedataParams
    baseDataFromCP?: BasedataParams
    onCancel: () => void
    className?: string
    textMessage?: string
    editHandler: () => void
    editItemKey: string | undefined
    error?: boolean
    errorMessaege?: string
    saveBaseData: (n: number[]) => void
}

export type CPBaseDataState = {
    baseDataIdArray: number[]
    checkedBoxs: boolean[]
    validationError: boolean
}

export default class CPBaseData extends React.PureComponent<CPBaseDataProps, CPBaseDataState> {
    constructor(props: CPBaseDataProps) {
        super(props)
        this.state = {
            baseDataIdArray: [],
            checkedBoxs: [],
            validationError: true,
        }
    }

    public getDerivedStateFromProps() {
        const checkMe: boolean[] = []
        const baseDataIdArrayMe: number[] = []
        this.props.baseDataFromServer.Data.sort(this.orderData).map(() => {
            checkMe.push(false)
        })
        this.props.baseDataFromServer.Data.map((sD: BasedataEntryParams, i: number) => {
            if (this.props.baseDataFromCP !== undefined) {
                this.props.baseDataFromCP.Data.map((cpD: BasedataEntryParams) => {
                    if (cpD.ID === sD.ID) {
                        if (baseDataIdArrayMe.indexOf(cpD.ID) < 0) {
                            baseDataIdArrayMe.push(cpD.ID)
                        }
                        checkMe[i] = true
                    }
                })
            }
        })
        this.setState(
            { baseDataIdArray: [...baseDataIdArrayMe], checkedBoxs: [...checkMe] },
            this.errorvalidationMessage,
        )
    }

    public render() {
        const { baseDataFromCP, baseDataFromServer } = this.props

        const baseDataContentEdit = (
            <Row>
                {baseDataFromServer.Data.sort(this.orderData).map(this.renderContentEdit)}
                {this.props.textMessage && (
                    <Col md={12}>
                        <span style={{ fontSize: '14px', color: '#666' }}>{this.props.textMessage}</span>
                    </Col>
                )}
            </Row>
        )

        const baseDataContent =
            baseDataFromCP && baseDataFromCP.Data.length > 0 ? (
                baseDataFromCP.Data.sort(this.orderData).map(this.renderContent)
            ) : (
                <Col className="mt-2">
                    <div>Es fehlt noch Ihre Angabe, bitte ausfüllen. </div>
                </Col>
            )

        return (
            <CPItemEditComponent
                name={this.props.baseDataFromServer.Name}
                editHandler={this.props.editHandler}
                saveHandler={() => {
                    if (!this.state.validationError) {
                        this.props.saveBaseData(this.state.baseDataIdArray)
                    }
                }}
                cancelHandler={this.props.onCancel}
                content={baseDataContent}
                contentEdit={baseDataContentEdit}
                editState={this.editStateThisComponent()}
                error={this.props.error}
                errorMessage={this.props.errorMessaege}
                validationError={this.state.validationError}
                validationMessage={this.state.validationError ? 'Bitte wählen Sie mindesten ein Feld aus' : ''}
            />
        )
    }

    private editStateThisComponent() {
        let myEditState: EditState
        if (this.props.editItemKey === this.props.baseDataFromServer.Key) {
            myEditState = EditState.This
        } else if (this.props.editItemKey === undefined) {
            myEditState = EditState.None
        } else {
            myEditState = EditState.Other
        }
        return myEditState
    }

    private renderContent = (data: BasedataEntryParams, index: number) => {
        return (
            <Col md={2} key={data.Content + (index + 1)}>
                {data.Content}
            </Col>
        )
    }

    private renderContentEdit = (data: BasedataEntryParams, index: number) => {
        return (
            <Col className="mb-2 mt-2" md={3} key={data.Content + (index + 1)}>
                <Input
                    className="mr-1"
                    name={`checkbox${index}`}
                    id={`checkbox${index}`}
                    type="checkbox"
                    checked={this.state.checkedBoxs[index]}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        const baseDataIdArrayMe: number[] = this.state.baseDataIdArray
                        const checkMe: boolean[] = this.state.checkedBoxs
                        checkMe[index] = e.target.checked
                        if (checkMe[index]) {
                            if (baseDataIdArrayMe.indexOf(data.ID) < 0) {
                                baseDataIdArrayMe.push(data.ID)
                            }
                        } else {
                            if (baseDataIdArrayMe.indexOf(data.ID) > -1) {
                                baseDataIdArrayMe.splice(baseDataIdArrayMe.indexOf(data.ID), 1)
                            }
                        }
                        this.setState(
                            {
                                baseDataIdArray: [...baseDataIdArrayMe],
                                checkedBoxs: [...checkMe],
                            },
                            this.errorvalidationMessage,
                        )
                    }}
                />
                <label htmlFor={`checkbox${index}`}>{data.Content}</label>
            </Col>
        )
    }

    private ifAllArraySame = (bool: boolean) => {
        if (bool) {
            return false
        } else {
            return true
        }
    }

    private errorvalidationMessage = () => {
        if (this.state.checkedBoxs.every(this.ifAllArraySame)) {
            this.setState({
                validationError: true,
            })
        } else {
            this.setState({
                validationError: false,
            })
        }
    }

    private orderData = (a: BasedataEntryParams, b: BasedataEntryParams) => {
        if (a.Content.toLocaleLowerCase() < b.Content.toLocaleLowerCase()) {
            return -1
        } else if (a.Content.toLocaleLowerCase() > b.Content.toLocaleLowerCase()) {
            return 1
        }
        return 0
    }
}
