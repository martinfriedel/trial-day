import * as React from 'react'
import { Alert, Button, Col, Input, Row } from 'reactstrap'
import FAIcon from './components/FAIcon'
import './CPItemEditComponent.css'

export interface CPItemEditComponentProps {
    cancelHandler: () => void
    content: React.ReactNode
    contentEdit: React.ReactNode
    deleteHandler?: () => void
    editHandler: () => void
    editState: EditState
    error?: boolean
    errorMessage?: string
    name?: string
    saveHandler: () => void
    validationError?: boolean
    validationMessage?: string
}

export enum EditState {
    None,
    This,
    Other,
}

export const CPItemEditComponent = (props: CPItemEditComponentProps) => {
    if (props.editState === EditState.None) {
        return (
            <div className="mt-2 mb-4 CPItemEditComponent">
                {props.name && (
                    <Row>
                        <Col>
                            <h4>{props.name}</h4>
                        </Col>
                    </Row>
                )}
                <Row className="myItem">
                    <Col md={11}>{!props.error && <Row className="mt-2 mb-2">{props.content}</Row>}</Col>
                    {props.deleteHandler ? (
                        <a className="iconButton" onClick={props.deleteHandler}>
                            <FAIcon mouseHover="löschen" icon="trash" />
                        </a>
                    ) : null}
                    <a className="iconButton " onClick={props.editHandler}>
                        <FAIcon mouseHover="bearbeiten" icon="edit" />
                    </a>
                    {props.error ? <Alert color="danger">{props.errorMessage}</Alert> : null}
                </Row>
            </div>
        )
    } else if (props.editState === EditState.This) {
        return (
            <div className="mt-2 mb-4 CPItemEditComponent">
                {props.name && (
                    <Row>
                        <Col>
                            <h4>{props.name}</h4>
                        </Col>
                    </Row>
                )}
                {props.validationError ? (
                    <Alert className="validationErrorAlert alert" color="danger">
                        {props.validationMessage}
                    </Alert>
                ) : null}
                <Row className="myItem editing">{props.contentEdit}</Row>
                <Row>
                    <Col md={12} className="text-right">
                        <Button className="mt-2 mb-1 mr-1" size="sm" onClick={props.cancelHandler}>
                            Abbrechen
                        </Button>

                        <Button className="mt-2 mb-1" size="sm" onClick={props.saveHandler}>
                            Speichern
                        </Button>
                    </Col>
                </Row>
            </div>
        )
    } else {
        return (
            <div className="mt-2 mb-4 CPItemEditComponent">
                {props.name && (
                    <Row>
                        <Col>
                            <h4>{props.name}</h4>
                        </Col>
                    </Row>
                )}
                <Row className="myItem">
                    <Col md={11}>{!props.error && <Row className="mt-2 mb-2">{props.content}</Row>}</Col>
                </Row>
                {props.error ? <Alert color="danger">{props.errorMessage}</Alert> : null}
            </div>
        )
    }
}

export interface SelectInputProps {
    onChangeOption: (e: React.ChangeEvent<HTMLInputElement>) => void
    selectArr: string[]
    children?: React.ReactNode
    defaultValue?: string
    className?: string
    bsSize?: 'lg' | 'sm'
    id?: string
    name?: string
    style?: React.CSSProperties
    inline?: boolean
    value?: string
}

export const SelectInput = (props: SelectInputProps) => {
    const classes = []
    if (props.className) {
        classes.push(props.className)
    }
    return (
        <>
            <>{props.children}</>
            <Input
                style={props.style}
                bsSize={props.bsSize}
                defaultValue={props.defaultValue}
                id={props.id}
                className={classes.join(' ')}
                type="select"
                value={props.value}
                inline={props.inline}
                name={props.name}
                onChange={props.onChangeOption}
            >
                {props.selectArr.map((selectVall: string, i: number) => (
                    <option key={i}>{selectVall}</option>
                ))}
            </Input>
        </>
    )
}
