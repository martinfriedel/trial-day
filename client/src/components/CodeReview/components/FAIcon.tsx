import * as React from 'react'

interface FAIconProps {
    icon: string
    color?: string
    className?: string
    mouseHover?: string
    onClick?: (e: React.MouseEvent<HTMLElement>) => void
}

export default class FAIcon extends React.Component<FAIconProps, any> {
    public render() {
        const { className, icon, mouseHover, onClick } = this.props

        const classes = ['fa']

        if (className) {
            classes.push(className)
        }

        classes.push('fa-' + icon)
        return <i title={mouseHover} className={classes.join(' ')} onClick={onClick ? onClick : undefined} />
    }
}
