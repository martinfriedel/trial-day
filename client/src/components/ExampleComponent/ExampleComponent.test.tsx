import { Button } from '@mui/material'
import ExampleComponent, { ExampleComponentProps } from 'components/ExampleComponent/ExampleComponent'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

const props: ExampleComponentProps = {
    currentValue: 0,
    decreaseValue: jest.fn(),
    increaseValue: jest.fn(),
}

describe('Example Component', () => {
    it('should render correctly', () => {
        const component = shallow(<ExampleComponent {...props} />)

        expect(toJson(component)).toMatchSnapshot()
    })

    it('should handle clicks correctly', () => {
        const decreaseValue = jest.fn()
        const increaseValue = jest.fn()
        const component = shallow(
            <ExampleComponent {...props} decreaseValue={decreaseValue} increaseValue={increaseValue} />,
        )
        component.find(Button).at(0).simulate('click')
        component.find(Button).at(1).simulate('click')
        expect(decreaseValue).toHaveBeenCalledTimes(1)
        expect(increaseValue).toHaveBeenCalledTimes(1)
    })
})
