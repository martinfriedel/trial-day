import { withKnobs } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import ExampleComponent from 'components/ExampleComponent/ExampleComponent'
import { useState } from 'react'

const stories = storiesOf('Example Component', module)

stories.addDecorator(withKnobs)
stories.add('default view', () => {
    const [value, setValue] = useState(0)
    return (
        <ExampleComponent
            currentValue={value}
            decreaseValue={() => setValue(value - 1)}
            increaseValue={() => setValue(value + 1)}
        />
    )
})
