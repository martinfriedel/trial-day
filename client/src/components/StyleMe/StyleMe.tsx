import { Box, InputBase, Typography } from '@mui/material'

export interface StyleMeProps {
    label: string
    value: string
    setValue: (value: string) => void
}

const StyleMe = (props: StyleMeProps): JSX.Element => {
    const { label, setValue, value } = props

    return (
        <Box>
            <Typography>{label}</Typography>
            <InputBase onChange={(e) => setValue(e.target.value)} value={value}></InputBase>
        </Box>
    )
}

export default StyleMe
