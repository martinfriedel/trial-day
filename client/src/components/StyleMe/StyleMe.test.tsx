import { InputBase } from '@mui/material'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import React from 'react'
// import React, { useState as useStateMock} from 'react'
// import { useHistory as useHistoryMock } from 'react-router-dom'
import StyleMe, { StyleMeProps } from './StyleMe'

// jest.mock('react-i18next', () => ({
//     useTranslation: (): unknown => ({ t: (key: string): string => key }),
// }))

// jest.mock('react-router-dom', () => ({
//       ...(jest.requireActual('react-router-dom') as Record<string, unknown>),
//     useHistory: (): unknown => ({
//         push: jest.fn(),
//     }),
// }))

//   jest.mock('react', () => ({
//       ...(jest.requireActual('react') as Record<string, unknown>),
//       useState: jest.fn(),
//   }))

const props: StyleMeProps = {
    label: 'Test label',
    value: 'Test value',
    setValue: jest.fn(),
}

describe('StyleMe Component', () => {
    //  beforeEach(() => {
    //    const setState = jest.fn()
    //   // @ts-expect-error: mocking the state
    //    useStateMock.mockImplementation((state) => [state, setState])
    //  })
    it('should render correctly with enzyme', () => {
        const component = shallow(<StyleMe {...props} />)

        expect(toJson(component)).toMatchSnapshot()
    })
    it('should handle a click correctly', () => {
        const onChangeMock = jest.fn()
        const component = shallow(<StyleMe {...props} setValue={onChangeMock} />)
        // console.log(component.debug()); // < - Use this to check the "snapshot" of a shallow
        component.find(InputBase).simulate('change', { target: { value: 'Another value' } })
        expect(onChangeMock).toHaveBeenCalledWith('Another value')
        expect(onChangeMock).toHaveBeenCalledTimes(1)
    })
})
