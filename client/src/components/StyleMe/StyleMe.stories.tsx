import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import React, { useState } from 'react'
import StyleMe, { StyleMeProps } from './StyleMe'

const stories = storiesOf('StyleMe', module)

const props: StyleMeProps = {
    setValue: action('setValue'),
    value: 'Test value',
    label: 'Test label',
}

stories.addDecorator(withKnobs)
stories.add('default view', () => {
    const [value, setValue] = useState('Test Value')
    return <StyleMe {...props} value={value} setValue={setValue} />
})
