import { Typography } from '@mui/material'
import { Box } from '@mui/system'
import ExampleComponent from 'components/ExampleComponent'
import { useState } from 'react'

const Example = (): JSX.Element => {
    const [value, setValue] = useState(0)

    return (
        <Box width={'100%'} minHeight={'100vh'} paddingTop={6}>
            <Typography component={'h1'} variant={'h4'} align={'center'}>
                Example page
            </Typography>
            <Box marginTop={5}>
                <ExampleComponent
                    currentValue={value}
                    decreaseValue={() => setValue(value - 1)}
                    increaseValue={() => setValue(value + 1)}
                />
            </Box>
        </Box>
    )
}

export default Example
