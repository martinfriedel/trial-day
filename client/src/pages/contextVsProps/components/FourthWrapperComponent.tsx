import { Card, CardContent, CardHeader, Typography } from '@mui/material'
import { Data1 } from 'pages/contextVsProps/types'

const FourthWrapperComponent = (props: Data1) => {
    const { address, age, name } = props

    return (
        <Card>
            <CardHeader
                title={name}
                titleTypographyProps={{
                    variant: 'h5',
                }}
            />
            <CardContent>
                <Typography>{age}</Typography>
                <Typography>{address.street}</Typography>
                <Typography>
                    {address.city}, {address.state}
                </Typography>
            </CardContent>
        </Card>
    )
}

export default FourthWrapperComponent
