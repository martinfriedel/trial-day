import ThirdWrapperComponent from 'pages/contextVsProps/components/ThirdWrapperComponent'
import { Data1 } from 'pages/contextVsProps/types'

const SecondWrapperComponent = (props: Data1) => {
    return <ThirdWrapperComponent {...props} />
}

export default SecondWrapperComponent
