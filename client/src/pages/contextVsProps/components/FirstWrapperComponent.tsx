import { Box, Typography } from '@mui/material'
import SecondWrapperComponent from 'pages/contextVsProps/components/SecondWrapperComponent'
import { Data1, Data2 } from 'pages/contextVsProps/types'

const FirstWrapperComponent = (props: { data1: Data1; data2: Data2 }) => {
    const { data1, data2 } = props
    const { carColor, carModel, carYear } = data2
    return (
        <Box>
            <Box display={'flex'} flexDirection={'row'} columnGap={1}>
                <Box>
                    <Typography>Car Model</Typography>
                    <Typography>{carModel}</Typography>
                </Box>
                <Box>
                    <Typography>Car Year</Typography>
                    <Typography>{carYear}</Typography>
                </Box>
                <Box>
                    <Typography>Car Color</Typography>
                    <Typography>{carColor}</Typography>
                </Box>
            </Box>
            <SecondWrapperComponent {...data1} />
        </Box>
    )
}

export default FirstWrapperComponent
