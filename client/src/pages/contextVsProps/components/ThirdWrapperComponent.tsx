import FourthWrapperComponent from 'pages/contextVsProps/components/FourthWrapperComponent'
import { Data1 } from 'pages/contextVsProps/types'

const ThirdWrapperComponent = (props: Data1) => {
    return <FourthWrapperComponent {...props} />
}

export default ThirdWrapperComponent
