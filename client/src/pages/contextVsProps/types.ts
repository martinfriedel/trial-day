export interface Data1 {
    name: string
    age: number
    address: {
        street: string
        city: string
        state: string
    }
}

export interface Data2 {
    carModel: string
    carYear: number
    carColor: string
}
