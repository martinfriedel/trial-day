import FirstWrapperComponent from 'pages/contextVsProps/components/FirstWrapperComponent'
import { Data1, Data2 } from 'pages/contextVsProps/types'

const ContextVsProps = () => {
    const data1: Data1 = {
        name: 'John Doe',
        age: 32,
        address: {
            street: '123 Main St',
            city: 'Anytown',
            state: 'CA',
        },
    }

    const data2: Data2 = {
        carModel: 'Ford',
        carYear: 2020,
        carColor: 'Red',
    }

    return <FirstWrapperComponent data1={data1} data2={data2} />
}

export default ContextVsProps
