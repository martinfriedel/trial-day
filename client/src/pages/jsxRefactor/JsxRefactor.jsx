import { Box } from '@mui/system'
import { InputSearch } from 'components/InputSearch/InputSearch'
import React from 'react'

const JSXRefactor = () => {
    const [input, setInput] = React.useState('')
    const [search, setSearch] = React.useState('')

    return (
        <Box width={'100%'} minHeight={'100vh'} paddingTop={6}>
            <p>Here you can search:</p>
            <InputSearch
                autoFocus={true}
                value={input}
                onChange={(e) => {
                    setInput(e.target.value)
                }}
                search={() => {
                    setSearch(innput)
                }}
            />
            {search && <h3>You searched for: {search}</h3>}
        </Box>
    )
}

export default JSXRefactor
