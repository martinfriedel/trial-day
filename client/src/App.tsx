import ContextVsProps from 'pages/contextVsProps'
import Example from 'pages/Example'
import GraphQL from 'pages/graphql'
import JSXRefactor from 'pages/jsxRefactor/JsxRefactor'
import { useState } from 'react'
import { HashRouter, Navigate, Route, Routes, useNavigate } from 'react-router-dom'

const BottomNavBar = (props: {
    navLinks: { path: string; label: string; component: JSX.Element }[]
    open: boolean
    setOpen: (payload: boolean) => void
}) => {
    const { navLinks, open, setOpen } = props
    const navigate = useNavigate()

    return (
        <div
            style={{
                position: 'fixed',
                bottom: open ? 0 : -50,
                left: 0,
                right: 0,
                height: 50,
                backgroundColor: 'teal',
                zIndex: 10000,
                display: 'flex',
                flexDirection: 'row',
                columnGap: '30px',
                alignItems: 'center',
                paddingLeft: '20px',
                paddingRight: '20px',
                transition: 'bottom 250ms ease-in-out',
            }}
        >
            <div
                style={{
                    position: 'absolute',
                    bottom: 50,
                    marginLeft: 'auto',
                    marginRight: 'auto',
                    width: '40px',
                    left: 0,
                    right: 0,
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    transition: 'bottom 250ms ease-in-out',
                }}
            >
                <div
                    onClick={() => setOpen(open ? false : true)}
                    style={{
                        width: '24px',
                        height: '24px',
                        backgroundColor: 'black',
                        cursor: 'pointer',
                    }}
                />
            </div>
            {navLinks.map((link) => (
                <div key={link.path}>
                    <div
                        onClick={() => navigate(link.path)}
                        style={{
                            borderRadius: '20%',
                            backgroundColor: 'orange',
                            cursor: 'pointer',
                            padding: '5px 10px',
                        }}
                    >
                        {link.label}
                    </div>
                </div>
            ))}
        </div>
    )
}

function App(): JSX.Element {
    const navLinks: { path: string; label: string; component: JSX.Element }[] = [
        {
            path: '/',
            label: 'Example',
            component: <Example />,
        },
        {
            path: '/jsxRefactor',
            label: 'JSXRefactor',
            component: <JSXRefactor />,
        },
        {
            path: '/graphql',
            label: 'GraphQL',
            component: <GraphQL />,
        },
        {
            path: '/contextVsProps',
            label: 'Context Vs Props',
            component: <ContextVsProps />,
        },
    ]

    const [open, setOpen] = useState(false)

    return (
        <HashRouter>
            <Routes>
                {navLinks.map((link) => (
                    <Route key={link.path} path={link.path} element={link.component} />
                ))}
                <Route element={<Navigate to={'/'} />} />
            </Routes>
            <BottomNavBar
                navLinks={navLinks}
                open={open}
                setOpen={(newOpen) => {
                    console.log(newOpen)
                    setOpen(newOpen)
                }}
            />
        </HashRouter>
    )
}

export default App
