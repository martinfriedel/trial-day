import { Typography } from '@mui/material'
import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'
import React from 'react'
// import React, { useState as useStateMock} from 'react'
// import { useHistory as useHistoryMock } from 'react-router-dom'
import * as renderer from 'react-test-renderer'
import {{ pascalCase name }}, { {{ pascalCase name }}Props }  from './{{pascalCase name}}'

jest.mock('react-i18next', () => ({
    useTranslation: (): unknown => ({ t: (key: string): string => key }),
}))


// jest.mock('react-router-dom', () => ({
//       ...(jest.requireActual('react-router-dom') as Record<string, unknown>),
//     useHistory: (): unknown => ({
//         push: jest.fn(),
//     }),
// }))

//   jest.mock('react', () => ({
//       ...(jest.requireActual('react') as Record<string, unknown>),
//       useState: jest.fn(),
//   }))


const props: {{ pascalCase name }}Props = {
    onClick: jest.fn(),
}


describe('{{ pascalCase name }} Component', () => {
//  beforeEach(() => {
//    const setState = jest.fn()
//   // @ts-expect-error: mocking the state
//    useStateMock.mockImplementation((state) => [state, setState])
//  })

  it('should render correctly with react-test-renderer', () => {
      const component = renderer.create(<{{ pascalCase name }} {...props} />)

      expect(component).toMatchSnapshot()
  })
  it('should render correctly with enzyme', () => {
    const component = shallow(<{{ pascalCase name }} {...props} />)
    
    expect(toJson(component)).toMatchSnapshot()
  });
  it('should handle a click correctly', () => {
    const onClickMock = jest.fn()
    const component = shallow(<{{ pascalCase name }} onClick={onClickMock} />)
    // console.log(component.debug()); // < - Use this to check the "snapshot" of a shallow
    component.find(Typography).simulate('click', { currentTarget: { value: 0 } })
    expect(onClickMock).toHaveBeenCalledTimes(1)
  })
})
