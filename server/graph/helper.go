package graph

func isNilOrEmpty(s *string) bool {
	return s == nil || *s == ""
}
