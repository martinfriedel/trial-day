package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"
	"testDay/graph/generated"
	"testDay/graph/model"
)

func (r *mutationResolver) SaveTodo(ctx context.Context, input model.NewTodo) (*model.Todo, error) {
	todo := &model.Todo{
		Done: *input.Done,
		Text: input.Text,
		User: &model.User{ID: input.UserID, Name: "user " + input.UserID},
	}
	if isNilOrEmpty(input.ID) {
		//create new
		todo.ID = fmt.Sprintf("T%d", len(r.Data)+1)
		r.Data = append(r.Data, todo)
	} else {
		todo.ID = *input.ID
		for i, v := range r.Data {
			if v.ID == *input.ID {
				r.Data[i].Text = todo.Text
				r.Data[i].User = todo.User
				r.Data[i].Done = todo.Done
			}
		}
	}
	return todo, nil
}

func (r *queryResolver) Todos(ctx context.Context) ([]*model.Todo, error) {
	return r.Data, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
