package main

import (
	"log"
	"net/http"
	"strconv"
	"testDay/graph"
	"testDay/graph/generated"
	"testDay/graph/model"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/Pallinder/go-randomdata"
)

const port = "8080"

func main() {
	initState := make([]*model.Todo, 100)
	for i := 0; i < 100; i++ {
		todo := &model.Todo{
			ID:   "T" + strconv.Itoa(i+1),
			Text: randomdata.SillyName(),
			Done: false,
			User: &model.User{
				ID:   strconv.Itoa(i),
				Name: randomdata.LastName(),
			},
		}
		initState[i] = todo
	}
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{Data: initState}}))

	http.Handle("/", playground.Handler("GraphQL playground", "/gql"))
	http.Handle("/gql", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
